section .data
%include 'colon.inc'
%include 'words.inc'
pointer: times 256 db 0
size_error_msg: db 'Word is more than 255 characters', 0
find_error_msg: db 'Cannot find your string in dictionary', 0

section .text

extern find_word
extern read_word
extern string_length
extern print_string
extern exit
global _start

_start:

	mov rdi, pointer
	mov rsi, 255
	call read_word
	test rax, rax
	je .size_error

	mov rdi, rax
	mov rsi, prev_word

	call find_word
	test rax, rax
	je .find_error

	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi

	call print_string

	mov rax, 1
	call exit

.size_error:
	mov rdi, size_error_msg
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	mov rsi, size_error_msg
	syscall
	mov rax, 0
	call exit

.find_error:
	mov rdi, find_error_msg
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	mov rsi, find_error_msg
	syscall
	mov rax, 0
	call exit
