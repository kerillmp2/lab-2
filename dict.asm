section .text

extern string_equals
extern print_string
global find_word

find_word:
	;rdi - pointer to string
	;rsi - pointer to the last word of dictionary



.check_equals:
	;skip prev_word pointer
	add rsi, 8

	push rsi
	push rcx
	push rdi
	call string_equals
	pop rdi
	pop rcx
	pop rsi

	test rax, rax
	jnz .return_true
	jmp .next_word

.next_word:
	;go to prev_word pointer
	sub rsi, 8
	mov rcx, [rsi]
	mov rsi, rcx
	test rsi, rsi
	jz .return_false
	jmp .check_equals

.return_false:
	mov rax, 0
	ret

.return_true:
	sub rsi, 8
	mov rax, rsi
	ret
