section .data

global string_length
global print_string
global print_newline
global print_int
global read_word
global parse_int
global string_equals
global string_copy
global exit

section .text

map_file:
	;rdi - filename
	mov rax, 2
	mov rsi, 0 ;read only
	mov rdx, 0
	syscall

	mov r8, rax
	mov rax, 9
	mov rdi, 0
	mov rsi, 32
	mov rdx, 0x1 ;protection read
	mov r10, 0x2 ;map private. Pages will not be shared
	mov r9, 0
	syscall

	ret ;returns mapped address

is_simple:
	;rdi - number
	mov rsi, 1	;current divider
	xor r8, r8	;number of dividers
.loop:
	cmp rsi, rdi
	jg .end
	mov rax, rdi
	xor rdx, rdx
	div rsi
	inc rsi
	cmp rdx, 0
	je .add_divider
	jmp .loop

.add_divider:
	inc r8
	jmp .loop

.end:
	cmp r8, 2
	je .return_true
	jmp .return_false

.return_true:
	mov rax, 1
	ret

.return_false:
	mov rax, 0
	ret

string_copy:
	;rdi - pointer to string
	;rsi - pointer to buffer
	;rdx - buffer's length
	xor rax, rax
.loop:
	xor rcx, rcx
	cmp rdx, rax
	je .return_zero
	mov cl, byte[rdi + rax]
	mov byte[rsi + rax], cl
	cmp cl, 0
	je .end
	inc rax
	jmp .loop
.return_zero:
	xor rax, rax
	ret
.end:
	mov rax, rdx
	ret

string_equals:
	;rdi - first string pointer
	;rsi - second string pointer
	;rdx - first string symbol
	;rcx - second string symbol

.loop:
	mov dl, byte[rdi]
	mov cl, byte[rsi]
	cmp dl, cl
	je .next_symbol
	mov rax, 0
	ret

.next_symbol:
	cmp dl, 0
	je .return_true
	inc rdi
	inc rsi
	jmp .loop

.return_true:
	mov rax, 1
	ret

parse_int:
	;rdi - string address
	dec rdi

.skip_whitespaces:
	inc rdi
	mov sil, byte[rdi]
	cmp byte[rdi], 0
	je .return_zero
	cmp byte[rdi], 0x20
	je .skip_whitespaces
	cmp byte[rdi], 0x9
	je .skip_whitespaces
	cmp byte[rdi], 0x10
	je .skip_whitespaces
	cmp byte[rdi], '-'
	je .negative_number
	cmp byte[rdi], '0'
	jb .return_zero
	cmp byte[rdi], '9'
	ja .return_zero

.positive_number:
	call parse_uint
	ret

.negative_number:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret

.return_zero:
	mov rax, 0
	mov rdx, 0
	ret


parse_uint:
	xor rax, rax
	call string_length
	mov rcx, rax
	xor rax, rax
	xor rdx, rdx

.read_number:
	xor rcx, rcx
	mov cl, byte[rdi+rdx]
	cmp cl, '0'
	jb .end
	cmp cl, '9'
	ja .end
	mov rsi, 10
	push rdx
	mul rsi
	pop rdx
	sub cl, '0'
	add rax, rcx
	inc rdx
	jmp .read_number
.end:
	ret

.return_zero:
	mov rax, 0
	mov rdx, 0
	ret

read_word:
	xor rcx, rcx

.skip_whitespaces:
	push rdi
	push rsi
	push rcx
	call read_char
	pop rcx
	pop rsi
	pop rdi
	cmp al, 9
	je .skip_whitespaces
	cmp al, 32
	je .skip_whitespaces

.write:
	mov byte[rdi + rcx], al
	cmp al, 0x10
	je .end
	cmp al, 0xA
	je .end
	inc rcx
	cmp rcx, rsi
	je .return_zero
	push rdi
	push rsi
	push rcx
	call read_char
	pop rcx
	pop rsi
	pop rdi
	jmp .write

.end:
	mov rax, 0
	mov rdx, rcx
	mov byte [rdi + rcx], 0
	mov rax, rdi
	ret

.return_zero:
	xor rax, rax
	ret



read_word2:
	;rdi - buffer adress
	;rsi - buffer size
	;rcx - counter

.skip_whitespaces:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi

	cmp al, 9
	je .skip_whitespaces
	cmp al, 32
	je .skip_whitespaces

	jmp .write_symbol

.read_loop:
	push rdi
	push rsi
	push rcx
	call read_char
	pop rcx
	pop rsi
	pop rdi

.write_symbol:

	mov byte[rdi+rcx], al

	cmp al, 0
	je .end
	cmp al, 9
	je .end
	cmp al, 32
	je .end
	inc rcx
	cmp rcx, rsi
	je .return_zero
	jmp .read_loop

.return_zero:
	xor rax, rax
	ret
.end:
	mov rax, 0
	mov byte[rdi + rcx], 0
	mov rax, rdi
	mov rdx, rcx
	ret

read_char:
	mov rax, 0
	mov rdi, 0
	push ax
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop ax

	ret

print_int:
	cmp rdi, 0
	jl .print_negative
	jmp .print_positive

.print_negative:
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	call print_uint
	ret

.print_positive:
	call print_uint
	ret


print_uint:
	mov rsi, rsp
	mov rax, rdi
	mov rdi, 10
	push byte 0
.loop:
	push word 0
	add rsp, 1
	xor rdx, rdx
	div rdi

	add rdx, '0'
	mov byte[rsp], dl

	cmp rax, 0
	je .print
	jmp .loop

.print:
	mov rdi, rsp
	push rsi
	call print_string
	pop rsi
	mov rsp, rsi
	ret

print_newline:

	mov rdi, 10
	call print_char

	ret

print_char:

	;Push char on the stack
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall

	pop rdi
	ret

print_string:
	;rdi - string pointer

	xor rax, rax
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall

	ret

string_length:
	;rdi - string pointer
	xor rax, rax

.loop:
	cmp byte [rdi+rax], 0x0
	je .end
	inc rax
	jmp .loop

.end:
	ret

exit:
	mov rax, 60
	syscall
