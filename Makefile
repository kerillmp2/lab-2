ASM = nasm
ASMF = -f elf64

lib.o: lib.asm
	$(ASM) $(ASMF) -o lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(ASMF) -o dict.o dict.asm

main.o: main.asm
	$(ASM) $(ASMF) -o main.o main.asm

prog: lib.o dict.o main.o
	ld -o prog lib.o dict.o main.o
