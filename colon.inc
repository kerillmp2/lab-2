%define prev_word 0

%macro colon 2
	%2: dq prev_word
	db %1, 0
	%define prev_word %2
%endmacro
